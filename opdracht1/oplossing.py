#!/usr/bin/python3

import os, multiprocessing, time

sema = [multiprocessing.Semaphore(1),multiprocessing.Semaphore(1)]
fmt = ["| %d |   |","|   | %d |"]

me=0
if os.fork():
	if os.fork():
		sema[0].acquire()
	else:
		me=1
	for i in range(1, 10):
		sema[me].acquire()
		print(fmt[me] % (2*i-me))
		time.sleep(1)
		sema[1-me].release()
