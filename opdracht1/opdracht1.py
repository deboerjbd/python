#!/usr/bin/python3
import os, multiprocessing, time

sema = [multiprocessing.Semaphore(1),multiprocessing.Semaphore(1)]
num = 0

if os.fork():
	if os.fork():
		sema[0].acquire()
	else:
		num=1

for i in range (1, 10):
	sema[num].acquire()
#	print(os.getpid() + 'meld nummer: ' + repr(num))
	print(os.getpid())
	print('meld nummer: ' + repr(num))
	num++
	time.sleep(1)
	sema[1-num].release()
os.exit
