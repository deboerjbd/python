#!/usr/bin/python3

myint1 = 1
myint2 = 5

myfloat1 = 4.3
myfloat2 = 3.7

mystring1 = 'what is your data?'
mystring2 = 'Jeffrey Python'

print(myint1 + myint2)
print(mystring1 + ' ' + mystring2)
print(myfloat1 + myfloat2)

print(mystring1 + ' ' + repr(myint1 + myint2))
