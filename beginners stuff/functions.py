#!/usr/bin/python3

myname = 'Jeffrey de Boer'

# Functie bij value.

#def functie1 (mystr):
# 'Dit is m\'n eerste functie'
# print('mystr is %s' % mystr)

#functie1(myname)

#def addnumbers(a,b):
# 'Voeg nummers toe'
# print('a = %d' % a)
# print('b = %d' % b)
# return a+b

#c = addnumbers(4,5)
#print('c = %d' % c)

# Functie bij reference.

a = [2]

def changenummer(num):
 'verander nummer in a'
 num.append(4)
 print('num = ')
 print(num)
 return

print('a = ')
print(a)
changenummer(a)
print('a = ')
print(a)
