#!/usr/bin/python3

#mystr = 'outside hello world'

#def main_function():
# mystr = 'Hello World'
# print('mystr = %s' % mystr)
# return

#imain_function()
#print('global: mystr = %s' % mystr)

mystr = 'Global mysql'

def global_function():
 mystr = 'Sub global mystr'
 def local_function():
  global mystr
  mystr = 'local mystr'
  return
 local_function()
 print('ub mystr = %s' % mystr)

global_function()
print('global mystr = %s' % mystr)
