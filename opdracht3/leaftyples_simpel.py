#!/usr/bin/python3

# Maak een leaf aan.
def make_leaf(symbol, weight):
	return (symbol, weight)

# Check of x een leaf is.
def is_leaf(x):
	return isinstance(x, tuple) and \
	len(x) == 2 and \
	isinstance(x[0], str) and \
	isinstance(x[1], int)

# Access functions.
# Access symbol.
def get_leaf_symbol(leaf):
	return leaf[0]

# Access Frequentie.
def get_leaf_freq(leaf):
	return leaf[1]


# [linker_branch, rechter_branch, symbols, freq]
# Get de linker tree. Int 0
def get_left_branch(huff_tree):
	return huff_tree[0]

# Get de rechter tree. Int 1
def get_right_branch(huff_tree):
	return huff_tree[1]

# Get voor ophalen symbol. Als we de left branch willen return eerste element. Anders tweede.
# Als we een symbol krijgen moeten we checken of het een leaf is. (anders krijgen we alleen leaf symbol)

def get_symbols(huff_tree):
	if is_leaf(huff_tree):
		return [get_leaf_symbol(huff_tree)]
	else:
		return huff_tree[2]

# Get voor freq. 
# Ook check of het een leaf is of niet.
def get_freq(huff_tree):
	if is_leaf(huff_tree):
		return [get_leaf_freq(huff_tree)]
	else:
		return huff_tree[3]

# Functie om een huffman tree aan te maken.
def make_huffman_tree(left_branch, right_branch):
	return [left_branch,
		right_branch,
		get_symbols(left_branch) + get_symbols(right_branch),
		get_freq(left_branch) + get_freq(right_branch)]

# Maak huffman tree aan. Doe ik hier even handmatig. 
ht01 = make_huffman_tree(make_leaf('A', 4),
				make_huffman_tree(make_leaf('B', 2),
					make_huffman_tree(make_leaf('D', 1),
						make_leaf('C', 1))))
